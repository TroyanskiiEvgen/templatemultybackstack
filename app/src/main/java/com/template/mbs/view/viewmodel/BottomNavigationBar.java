package com.template.mbs.view.viewmodel;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

import com.template.mbs.model.TabData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Relax on 06.06.2017.
 */

public class BottomNavigationBar extends TabLayout implements TabLayout.OnTabSelectedListener {

    private List<TabData> tabData;
    private TabSelectListener tabSelectionListener;

    public BottomNavigationBar(Context context) {
        super(context);
        setup();
    }

    public BottomNavigationBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public BottomNavigationBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    private void setup(){
        tabData = new ArrayList<>();
        addOnTabSelectedListener(this);
    }

    public BottomNavigationBar addItem(TabData tabData) {
        this.tabData.add(tabData);
        return this;
    }

    public void initialise() {
        for (int i = 0; i < tabData.size(); i++) {
            TabLayout.Tab tab = newTab();
            tab.setCustomView(new BottomNavigationTab(getContext(), tabData.get(i)));
            addTab(tab);
        }
    }


    public void selectTab(int position) {
        Tab t = getTabAt(position);
        if (t != null) {
            t.select();
        }
    }


    @Override
    public void onTabSelected(Tab tab) {
        if (tabSelectionListener != null) tabSelectionListener.onTabSelected(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab) {

    }

    @Override
    public void onTabReselected(Tab tab) {

    }

    public void setTabSelectedListener(TabSelectListener listener) {
        this.tabSelectionListener = listener;
    }

    public interface TabSelectListener{
        void onTabSelected(int tabPosition);
    }
}
