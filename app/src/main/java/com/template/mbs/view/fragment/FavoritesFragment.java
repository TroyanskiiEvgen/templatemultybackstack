package com.template.mbs.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.template.mbs.R;
import com.template.mbs.view.activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Relax on 08.06.2017.
 */

public class FavoritesFragment extends Fragment {

    private static final String TAG = FavoritesFragment.class.getSimpleName();;

    @BindView(R.id.fragment_text)
    TextView fragmentText;

    int fragmentNumber;

    public static FavoritesFragment newInstance(int fragmentCounter) {
        Bundle args = new Bundle();
        args.putInt(TAG, fragmentCounter);
        FavoritesFragment fragment = new FavoritesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, v);
        if (savedInstanceState != null) {
            fragmentNumber = savedInstanceState.getInt(TAG);
            fragmentText.setText(TAG + fragmentNumber);
        } else if (getArguments() != null) {
            fragmentNumber = getArguments().getInt(TAG);
            fragmentText.setText(TAG + fragmentNumber);
        }
        return v;
    }

    @OnClick(R.id.next)
    public void onNextlick() {
        ((MainActivity)getActivity()).showFragment(FavoritesFragment.newInstance(1 + fragmentNumber));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAG, fragmentNumber);
        super.onSaveInstanceState(outState);
    }
}