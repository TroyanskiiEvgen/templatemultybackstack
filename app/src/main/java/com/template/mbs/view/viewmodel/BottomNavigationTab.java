package com.template.mbs.view.viewmodel;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.template.mbs.R;
import com.template.mbs.model.TabData;

import butterknife.BindView;

/**
 * Created by Relax on 06.06.2017.
 */

public class BottomNavigationTab extends BaseTab {

    @BindView(R.id.tab_icon) ImageView image;
    @BindView(R.id.tab_title) TextView title;


    public BottomNavigationTab(Context context, TabData tabData) {
        super(context, tabData);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.view_bottom_nav_tab;
    }


    @Override
    protected void setupViews(TabData tabData, boolean isSelected) {
        image.setImageResource(isSelected ? tabData.getImageSelected() : tabData.getImageDefault());
        title.setText(tabData.getTitle());
        title.setTextColor(ContextCompat.getColor(getContext(),
                isSelected ? tabData.getSelectedColor() : tabData.getDefaultColor()));
    }
}
